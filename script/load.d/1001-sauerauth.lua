-- sauerworld keys

engine.writelog("Adding sauerworld auth users.")

-- admins
cs.adduser("mefisto", "sauerworld", "-d9651d600b8b9b86297ab330f55793f0f0b845f37d693c14", "a")
cs.adduser("swatllama", "sauerworld", "+aa2974bb0022a178f85a4787bf545b8a7a8383bc4388bf93", "a")

-- masters
cs.adduser("phobo", "sauerworld", "-70a78a7db3034de59747e0571c43ba01d005ab17593c8d7e", "m")
cs.adduser("firefly", "sauerworld", "-171a409663c7fa420990684d4bc555f1fe6cf5ea7a080cc", "m")
cs.adduser("bandandit", "sauerworld", "-b07a0f9a62ecc1f1188772b5b00fa669f4dc63b905b05b13", "m")
cs.adduser("notas", "sauerworld", "+a68e744184a7120ff454203c63d7a9c1279e0ee79ea22a7b", "m")
cs.adduser("avior", "sauerworld", "+2a0aecc9f5a68817512e453592d8e11bccc0acd96b7ecd7", "m")
cs.adduser("boofy", "sauerworld", "+a4037c725de2e40167d4ffdeab3609cbc4647fcc446d8400", "m")
cs.adduser("midnyte", "sauerworld", "+f0a943ddc4580357fed3b67a5c33360b298163c5de102a29", "m")
cs.adduser("paklet", "sauerworld", "-81765b71e41d4bfb1d2988ab745ba4301b55619fa4f5116f", "m")
cs.adduser("nooba", "sauerworld", "-6ca860f7142448b521ba8214dda3eb4992439285d1a37464", "m")
